var BATTLE = 'Battle';// const

function Options() { // Main options
    var params = {
        menu: {
            info: {
                generator: 'menuGenerator',
                steps: {
                    title: 'Back',
                    last_step: null
                },
                title: 'Main Menu',
            },
            single: {
                title: 'Single player',
                child: {
                    human: {
                        title: 'Human'
                    },
                    computer: {
                        title: 'Computer',
                        child: {
                            title: 'Difficult',
                            easy: {
                                title: 'Easy'
                            },
                            hard: {
                                title: 'Hard'
                            },
                            darth_vader: {
                                title: 'Darth Vader'
                            }
                        }
                    }
                }
            },
            multi: {
                title: 'Multiplayer(BETA)'
            }
        }
    }
    
    return params;
}