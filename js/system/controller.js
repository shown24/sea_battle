;var Main_controller;(function() {


    Main_controller = function(el) {

        var self = this;
        this.opt = {
            players: {},
            options: new Options(),
            startPoint: 'Home',
            gameParts: null,
            gameContainer: document.getElementById('sea_battle')
        }

    }

    Main_controller.prototype = new View();
    Main_controller.prototype.constructor = Main_controller;


    Main_controller.prototype.Start = function () {
        this.Router(this.getProgress());
        return this;
    }
    Main_controller.prototype.Router = function (gameInf) {
        this.getView(gameInf);
        this.drawAppParts(gameInf);
    }

    Main_controller.prototype.getProgress = function () {  // загружаем игру
        var res;
        if(localStorage.getItem(BATTLE)) {
            res = JSON.parse(localStorage.getItem(BATTLE));
        } else {
            res = this.opt;
        }

        return res;

    }

    //Main_controller.prototype.setProgress = function () {  // сохраняем игру
    //    if (this.progress)
    //        localStorage[BATTLE] = this.progress;
    //    return this;
    //}
    //
    //Main_controller.prototype.resetBattle = function () { // обновляем игру
    //    if (localStorage.getItem(BATTLE))
    //        localStorage.removeItem(BATTLE)
    //    return this;
    //}
    //
    //Main_controller.prototype.getOptions = function () {
    //}
    //
    //Main_controller.prototype.setOptions = function () {
    //}

})();