;var Core;(function() {

    Core = function() {

        this.gameParts = {
            game: false,
            parts: false,
            game_info: {
                game_type: {
                    single: {

                    }
                }
            }
        }

    }

    Core.prototype.drawAppParts = function(draw){
        console.log(draw);

        draw.gameContainer.innerHTML = draw.gameParts[draw.startPoint].outerHTML;
    }

    Core.prototype.menuGenerator = function(o){
        //console.log(o);
        var list = '';
        var block = document.createElement('span');
        block.setAttribute('class','main_menu');


        var title = document.createElement('span');
        title.setAttribute('class','main_menu__title');
        title.innerText = o.options.menu.info.title;

        block.appendChild(title);

        var name = (o.options.menu.info.steps.last_step) ? o.options.menu.info.steps.last_step : 'single';

        function generateList(n){

            //if(name === 'single')
            for(var k in o.options.menu){
                if(k === 'info')
                continue;

                var menu_el = document.createElement('span');
                menu_el.setAttribute('class','main_menu__nav');
                menu_el.setAttribute('data-goto', k);
                menu_el.innerText = o.options.menu[k].title;

                block.appendChild(menu_el);
            }
        }
        generateList(name);

        return block;

    }

})();
