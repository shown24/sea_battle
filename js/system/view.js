;var View;(function(){

    View = function(){

    }

    View.prototype = new Core();
    View.prototype.constructor = View;

    View.prototype.getView = function(o){
        if(o.gameParts && o.gameParts[o.startPoint]) {

            return o.gameParts[o.startPoint];

        } else {

            return this.generateView(o);

        }

    };

    View.prototype.generateView = function(o){
        if(!o.gameParts)
            o.gameParts = {};

        var res = new window[o.startPoint](o);
        o.gameParts[o.startPoint] = res;
        return o;
    }


    View.prototype.generateDomObject = function(c){
        var el = this[c.options.menu.info.generator](c);
        return el;
    }


    View.prototype.loadGame = function(template,gameObj){
        var res = new Object.create(template,gameObj);
        return res;
    }

})();